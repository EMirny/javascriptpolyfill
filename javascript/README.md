# Supports

* Array.includes()
* Element.closest()
* Element.matches()
* NodeList.forEach()
* Object.assign()
* String.includes()